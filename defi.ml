(*
  ocamlbuild -use-ocamlfind defi.native

  Given a dictionary and a start- and end-word, this program will look
  for a path between the two words, where there is an edge between A and B if:
  - A is an anagram of some A'
  - such that C can be reached from A' by adding, removing or replacing a letter
*)

open Batteries

(** Words *)

type word = {
  (* raw: the word *)
  raw : UTF8.t;
  (* bag: the word as a multiset of letters,
          in fact a sorted string *)
  bag : UTF8.t;
}

module CMap = Map.Make(UChar)

(* sort the letters in the word *)
let bag_of_raw raw =
  let chars = ref [] in
  UTF8.iter (fun c -> chars := c::!chars) raw;
  let chars = List.sort UChar.compare !chars in
  let buf = UTF8.Buf.create (UTF8.length raw) in
  List.iter (UTF8.Buf.add_char buf) chars;
  UTF8.Buf.contents buf

(* utility functions to traverse UTF8 strings *)
let after_end = UTF8.out_of_range
let next = UTF8.next
let get = UTF8.look

let word_of_raw raw =
  let bag = bag_of_raw raw in
  { bag; raw }

let char_before c = UChar.(of_int @@ int_of c - 1)
let char_after  c = UChar.(of_int @@ int_of c + 1)

(** Dictionaries as tries *)

(* we represent a dictionary by a lexicographic tree (trie)
     http://en.wikipedia.org/wiki/Prefix_tree *)
type 'a trie = Node of 'a option * 'a trie CMap.t

let empty = Node (None, CMap.empty)

let dico_of_path path =
  let rec add bag i w (Node (n, children)) =
    if after_end bag i then
      (* if there was already a word in this position, we may override
         it with another word which is an anagram; this is not an
         issue as all our search is done modulo anagrams *)
      Node (Some w, children)
    else
      let c = get bag i in
      let i' = next bag i in
      Node (n, CMap.modify_def empty c (add bag i' w) children)
  in
  let add_raw trie raw =
    let word = word_of_raw raw in
    add word.bag 0 word trie in
  let in_chan = open_in path in
  IO.lines_of in_chan
  |> Enum.fold add_raw empty
  |> tap (fun _ -> close_in in_chan)

(** Visiting the 1-neighbors of a node *)

(* Iterating on a map, or subparts of a map *)

let iter f m = CMap.iter (fun k v -> f v) m

let upto c m =
  let m, _after_c, _after = CMap.split (char_after c) m in m

let from c m =
  let _before, _before_c, m = CMap.split (char_before c) m in m

(* Iterating subnodes of a trie *)

let all_children (Node (_, children)) k =
  iter k children

let interval_to to_child (Node (_, children)) k =
  children |> upto to_child |> iter k

let interval_from from_child (Node (_, children)) k =
  children |> from from_child |> iter k

let interval from_child to_child (Node (_, children)) k =
  children |> from from_child |> upto to_child |> iter k

let follow_char child (Node (_, children)) k =
  match CMap.Exceptionless.find child children with
    | None -> ()
    | Some t -> k t

let rec follow_from bag i trie k =
  if after_end bag i then k trie
  else
    follow_char (get bag i) trie @@ fun trie ->
    follow_from bag (next bag i) trie k

(* A position in a word is not a [char] but a [char option]: [None]
   represents the void before or after the word *)
let follow pos trie k = match pos with
  | None -> k trie
  | Some char -> follow_char char trie k

let before pos = Option.map char_before pos
let after pos = Option.map char_after pos

let interval from_pos to_pos trie k = match from_pos, to_pos with
  | Some from_node, Some to_node -> interval from_node to_node trie k
  | Some from_node, None -> interval_from from_node trie k
  | None, Some to_node -> interval_to to_node trie k
  | None, None -> all_children trie k

(* the three edition operations *)

let replace prev cur next trie k =
  interval prev next trie k

let add cur next trie k =
  follow cur trie @@ fun child ->
  interval cur next child k

let remove cur trie k =
  if cur <> None then k trie

(* visiting the neighbors; those distant from a given bag
   by exactly one edition operation *)

let visit bag trie k =
  let rec edit bag prev cur i trie k =
    let next, i' =
      if after_end bag i then None, i
      else (Some (get bag i), next bag i) in
    (* either we do an edition on the current letter,
       and continue with the rest of the word unchanged *)
    let continue trie = follow_from bag i trie k in
    remove cur trie continue;
    add cur next trie continue;
    replace prev cur next trie continue;
    (* or we follow the current letter unchanged,
       and do an edition on the rest of the word *)
    if next <> None then begin
      follow cur trie @@ fun trie ->
      edit bag cur next i' trie k
    end
  in
  if after_end bag 0
  then edit bag None None 0 trie k
  else edit bag None (Some (get bag 0)) (next bag 0) trie k

(** BFS traversal *)

let rec map_trie f (Node (v, children)) =
  Node (f v, CMap.map (map_trie f) children)

let modify word f trie =
  let rec modify bag i f (Node (v, children)) =
    if after_end bag i then Node (f v, children)
    else
      let c = get bag i in
      let i' = next bag i in
      Node (v, CMap.modify c (modify bag i' f) children)
  in modify word.bag 0 f trie

let find word trie =
  let found = ref None in
  follow_from word.bag 0 trie
    (fun (Node (v, _)) -> found := v);
  !found

let collect_neighbors trie word =
  let list = ref [] in
  visit word.bag trie
    (fun (Node (v, children)) -> list := v :: !list);
  !list

(* we remember the path from the startpoint to any traversed node *)
type path = word list

let bfs trie start stop =
  if find start trie = None then failwith
    (Printf.sprintf "The start word (%s) is not in the trie" start.raw);
  if find stop trie = None then failwith
    (Printf.sprintf "The stop word (%s) is not in the trie" stop.raw);
  let module Node = struct
    type t = {
      word : word;
      is_stop : bool;
      mutable path: path option;
      (* if 'path' is None, the node has not yet been traversed;
         else it contains the reverse path of nodes reaching it
         from the starting node *)
    }
    exception Found of path
  end in
  let open Node in
  let trie =
    let node word = { word; is_stop = false; path = None } in
    trie
    |> map_trie (Option.map node)
    |> modify stop
        (Option.map (fun node -> {node with is_stop = true}))
    |> modify start
        (Option.map (fun node -> {node with path = Some [start]}))
  in
  try
    let queue = Queue.create () in
    Queue.add (start, [start]) queue;
    while true do
      let (word, rev_path) = Queue.pop queue in
      let add_neighbor (Node (n, _)) =
        match n with
          | None -> () (* no word there *)
          | Some { path = Some _; _ } -> () (* already visited *)
          | Some node ->
            let rev_path = node.word :: rev_path in
            node.path <- Some rev_path;
            if node.is_stop then
              raise (Found (List.rev rev_path));
            Queue.add (node.word, rev_path) queue;
      in
      visit word.bag trie add_neighbor
    done; None
  with
    | Queue.Empty -> None
    | Node.Found path -> Some path


(** Command-line interface *)
let search dico_path raw_start raw_stop =
  let dico, start, stop =
    dico_of_path dico_path,
    word_of_raw raw_start,
    word_of_raw raw_stop in
  try
    (match bfs dico start stop with
    | None -> print_endline "No path found."
    | Some path -> List.iter (fun {raw;_} -> print_endline raw) path)
  with Failure err ->
    Printf.printf "Error: %s\n" err

let () =
  let open Cmdliner in
  let dico =
    let doc = "dictionary file" in
    Arg.(required & pos 0 (some file) None & info [] ~docv:"DICO" ~doc) in
  let start =
    let doc = "starting word" in
    Arg.(required & pos 1 (some string) None & info [] ~docv:"START" ~doc) in
  let goal =
    let doc = "word to reach" in
    Arg.(required & pos 2 (some string) None & info [] ~docv:"GOAL" ~doc) in
  ignore @@ Term.eval
    (Term.(pure search $ dico $ start $ goal),
     Term.info "defi" ~doc:"Finds a path between two words in a dictionary")
